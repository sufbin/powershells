# Configure the settings to use to setup this Jenkins Executor
$Port = 80
$IPAddress = '192.168.1.96'
$SubnetPrefixLength = 24
$DNSServers = @('192.168.1.1')
$DefaultGateway = '192.168.1.1'


#bitbucket variables 
$bbt_url = 'https://sufbin@bitbucket.org/sufbin'
$jenk_bbt_url = "$bbt_url/jenk.git"

$win_folder = 'c:\ace'
$jenkins_folder = "$win_folder\jenkins"

# check and delete local folders
If(test-path $jenkins_folder)
{
	Remove-Item -Recurse -Force $jenkins_folder
}


# create local folders
New-Item -ItemType Directory $jenkins_folder
#download jCasC files
# download ace source code from bitbucket
Start-Process "C:\Program Files\Git\bin\git.exe" -argumentlist "clone $jenk_bbt_url $jenkins_folder"
#Invoke-WebRequest -Uri https://bitbucket.org/sufbin/jenk/raw/f35b2896203bf59dea16b53e239e8c66040e61dc/jenkins.yaml -OutFile c:\ace\jenkins\jenkins.yaml
#Invoke-WebRequest -Uri https://bitbucket.org/sufbin/jenk/raw/f35b2896203bf59dea16b53e239e8c66040e61dc/adminpw -OutFile c:\ace\jenkins\adminpw


# By default powershell uses TLS 1.0 the site security requires TLS 1.2 â€“ 
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Install .NET Framework 3.5
Install-WindowsFeature -Name NET-Framework-Core

# Install Chocolatey
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

# Install JDK 8 
Start-Process "choco" -argumentlist "install jdk8 -y" -wait

# Install Jenkins using Chocolatey
#Start-Process "choco" -argumentlist "install Jenkins -y" -wait
#choco install Jenkins -y
Invoke-WebRequest -Uri https://get.jenkins.io/war-stable/2.303.1/jenkins.war -OutFile c:\ace\jenkins\jenkins.war

New-NetFirewallRule -Name jenkins -DisplayName 'Jenkins' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 8080

#install plugin
#install plug-in manager
Invoke-WebRequest -Uri https://github.com/jenkinsci/plugin-installation-manager-tool/releases/download/2.11.0/jenkins-plugin-manager-2.11.0.jar -OutFile c:\ace\jenkins\jenkins-plugin-manager.jar


#set jenkins_home env variable
[System.Environment]::SetEnvironmentVariable('JENKINS_HOME','C:\ace\jenkins\.jenkins')

#start jenkins.war
#$HTTP_HOST=(Invoke-WebRequest -Uri http://ifconfig.me/ip -UseBasicParsing).Content.Trim()
#java -jar c:\ace\jenkins\jenkins.war --httpListenAddress=$HTTP_HOST
Start-Process "C:\Program Files\Java\jdk1.8.0_211\bin\java.exe" -argumentlist "-jar $jenkins_folder\jenkins.war --httpListenAddress=0.0.0.0"

Start-Sleep -s 15

#install jcasc plugin
#java -jar $jenkins_folder\jenkins-plugin-manager.jar --war "C:\Program Files (x86)\Jenkins\jenkins.war" --plugins configuration-as-code:1.51 git-client:3.6.0 git:4.6.0 credentials:2.6.1 job-dsl:1.77 workflow-job:2.41 workflow-cps:2.93 bitbucket:1.1.29 mailer:1.34 --plugin-download-directory "C:\Program Files (x86)\Jenkins\plugins"
Start-Process "C:\Program Files\Java\jdk1.8.0_211\bin\java.exe" -argumentlist "-jar $jenkins_folder\jenkins-plugin-manager.jar --war $jenkins_folder\jenkins.war --plugins configuration-as-code:1.51 git-client:3.6.0 git:4.6.0 credentials:2.6.1 job-dsl:1.77 workflow-job:2.41 workflow-cps:2.93 bitbucket:1.1.29 mailer:1.34 --plugin-download-directory C:\ace\jenkins\plugins" -NoNewWindow -Wait

#set JCasC env variable
[System.Environment]::SetEnvironmentVariable('CASC_JENKINS_CONFIG','c:\ace\jenkins\jenkins.yaml')

#move plug-in files from default folder to jenkins folder
Get-ChildItem -Path "C:\ace\jenkins\plugins" -Recurse -File | Move-Item -Destination "C:\ace\jenkins\.jenkins\plugins"

#stop jenkins.war
Stop-Process -name "*Java*" -Force

#restart jenkins.war
Start-Process "C:\Program Files\Java\jdk1.8.0_211\bin\java.exe" -argumentlist "-jar $jenkins_folder\jenkins.war --httpListenAddress=0.0.0.0"


#set JCasC env variable
#[System.Environment]::SetEnvironmentVariable('CASC_JENKINS_CONFIG','c:\ace\jenkins\jenkins.yaml')

#Restart-Service -Name Jenkins

# Set the port Jenkins uses
# $Config = Get-Content `
#   -Path "${ENV:ProgramFiles(x86)}\Jenkins\Jenkins.xml"
# $NewConfig = $Config `
#   -replace '--httpPort=[0-9]*\s',"--httpPort=$Port "
# Set-Content `
#   -Path "${ENV:ProgramFiles(x86)}\Jenkins\Jenkins.xml" `
#   -Value $NewConfig `
#   -Force
# Restart-Service `
#   -Name Jenkins