#Step - create variables
#Step - Install iis
#Step - Install .net
#Step - check and Install git


#Step - create variables
#bitbucket variables 
$bbt_url = 'https://sufbin@bitbucket.org/sufbin'
$gitInf_bbt_url = "$bbt_url/powershells/raw/master/git.inf"

$temp_win_folder
#local dir variables
$win_folder = 'c:\ace'
$temp_win_folder = "$win_folder\temp"
#------------------------

#------------------------
#Step - manage website folders 
# check and delete local folders
If(test-path $webapp_sit_win_folder)
{
	Remove-Item -Recurse -Force $webapp_sit_win_folder
	Remove-Item -Recurse -Force $temp_win_folder
}
# create local folders
New-Item -ItemType Directory $webapp_sit_win_folder
New-Item -ItemType Directory $temp_win_folder
#------------------------


# By default powershell uses TLS 1.0 the site security requires TLS 1.2 – 
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#------------------------
#Step - Install iis
Install-WindowsFeature web-server -IncludeManagementTools
#------------------------

#------------------------
#Step - Install .net
Install-WindowsFeature Web-Asp-Net45
# Install dot.net runtime
#Invoke-WebRequest https://go.microsoft.com/fwlink/?LinkId=2085155 -outfile c:\temp\ndp48-web.exe
#Start-Process c:\temp\ndp48-web.exe -ArgumentList '/quiet' -Wait

# Install dot.net sdk(optional)
# Invoke-WebRequest https://download.visualstudio.microsoft.com/download/pr/c1bfbb13-ad09-459c-99aa-8971582af86e/61553270dd9348d7ba29bacfbb4da7bd/dotnet-sdk-5.0.400-win-x64.exe -outfile c:\temp\dotnet-sdk-5.0.400-win-x64.exe
# Start-Process c:\temp\dotnet-sdk-5.0.400-win-x64.exe -ArgumentList '/quiet' -Wait
#------------------------

#------------------------
#Step - check and Install git
$FileName = "C:\Program Files\Git\bin\git.exe"
if (!(Test-Path $FileName) )
{
	$architecture = '64-bit'
	$assetName = "Git-*-$architecture.exe"
	$gitHubApi = 'https://api.github.com/repos/git-for-windows/git/releases/latest'
	$response = Invoke-WebRequest -Uri $gitHubApi -UseBasicParsing
	$json = $response.Content | ConvertFrom-Json
	$release = $json.assets | Where-Object Name -like $assetName
	$install_args = "/SP- /VERYSILENT /SUPPRESSMSGBOXES /NOCANCEL /NORESTART /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /LOADINF=""$gitInf_bbt_url"""
	Invoke-WebRequest $release.browser_download_url -OutFile "$temp_win_folder\$($release.name)"
	Start-Process -FilePath $temp_win_folder\$($release.name) -ArgumentList $install_args -Wait
}
#------------------------